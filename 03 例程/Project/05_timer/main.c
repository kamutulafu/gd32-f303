/*!
    \file  main.c
    \brief USART printf demo
*/

/*
    Copyright (C) 2017 GigaDevice

    2017-05-19, V1.0.0, demo for GD32F30x
*/

#include "gd32f30x.h"
#include "gd32f303e_eval.h"
#include "systick.h"
#include <stdio.h>

#include "led.h"
#include "timer.h"



void led_init(void);
void led_flash(int times);

/*!
    \brief      main function
    \param[in]  none
    \param[out] none
    \retval     none
*/
int main(void)
{
    /* initialize the LEDs */
    led_init();
    
    /* configure systick */
    systick_config();

	timer1_init();
    while(1){
        
    }
}


