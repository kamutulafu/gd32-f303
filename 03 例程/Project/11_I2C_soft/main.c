/*!
    \file  main.c
    \brief use the I2C bus to write and read EEPROM
*/

/*
    Copyright (C) 2017 GigaDevice

    2017-05-19, V1.0.0, demo for GD32F30x
*/

#include <stdio.h>
#include "gd32f30x.h"
#include "gd32f303e_eval.h"
#include "systick.h"
#include "i2c.h"
#include "at24cxx.h"

uint8_t count=0;

void led_turn_on(uint8_t led_number);
void led_config(void);

/*!
    \brief      configure the LEDs
    \param[in]  none
    \param[out] none
    \retval     none
*/
void led_config(void)
{
    gd_eval_led_init(LED2);
    gd_eval_led_init(LED3);
    gd_eval_led_init(LED4);
    gd_eval_led_init(LED5);

    /* turn off LED2,LED3,LED4,LED5 */
    gd_eval_led_off(LED2);
    gd_eval_led_off(LED3);
    gd_eval_led_off(LED4);
    gd_eval_led_off(LED5);
}

uint8_t test_data_w[] = {0x11 ,0x22 ,0x33};
uint8_t test_data_r[200];


/*!
    \brief      main function
    \param[in]  none
    \param[out] none
    \retval     none
*/
int main(void)
{   
    /* configure systick */
    systick_config();
    
    /* configure LEDs */
    led_config();
    
    /* configure USART */
    gd_eval_com_init(EVAL_COM1);

	IIC_Init();

	AT24C02_Write(0, test_data_w, sizeof(test_data_w));

	AT24C02_Read(0,test_data_r,3);
	printf("read at24c02 data is [0x%x] [0x%x] [0x%x]", test_data_r[0], test_data_r[1],test_data_r[2]);
	

    /* turn on all LEDs */
    gd_eval_led_on(LED2);
    gd_eval_led_on(LED3);
    gd_eval_led_on(LED4);
    gd_eval_led_on(LED5);
    
    while(1);

}

/*!
    \brief      trun on a LED
    \param[in]  led_number
    \param[out] none
    \retval     none
*/
void led_turn_on(uint8_t led_number)
{
    switch(led_number){
    case 0:
      gd_eval_led_on(LED2); 
      break;
    case 1:
      gd_eval_led_on(LED3);  
      break;
    case 2:
      gd_eval_led_on(LED4);
      break;
    case 3:
      gd_eval_led_on(LED5);
      break; 
    default:
      gd_eval_led_on(LED2);
      gd_eval_led_on(LED3);
      gd_eval_led_on(LED4);
      gd_eval_led_on(LED5);
      break;
    }
}

/* retarget the C library printf function to the usart */
int fputc(int ch, FILE *f)
{
    usart_data_transmit(EVAL_COM1, (uint8_t) ch);
    while (RESET == usart_flag_get(EVAL_COM1,USART_FLAG_TBE));
    return ch;
}
