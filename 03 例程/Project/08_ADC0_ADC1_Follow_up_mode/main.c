/*!
    \file  main.c
    \brief ADC0_ADC1_Follow_up_mode demo
*/

/*
    Copyright (C) 2017 GigaDevice

    2017-05-19, V1.0.0, demo for GD32F30x
*/

#include "gd32f30x.h"
#include "gd32f303e_eval.h"
#include "systick.h"
#include <stdio.h>

#include "adc.h"


void rcu_config(void);

/*!
    \brief      main function
    \param[in]  none
    \param[out] none
    \retval     none
*/
int main(void)
{
    /* system clocks configuration */
    rcu_config();
    /* configure systick */
    systick_config();  
    /* configure COM port */
    gd_eval_com_init(EVAL_COM1);

	adc_gpio_config();
	adc_timer_init();
	adc_dma_init();
	adc_init();
	
    while(1){
        delay_1ms(2000);
        printf(" the data adc_value[0] is %08X \r\n",adc_value[0]);
        printf(" the data adc_value[1] is %08X \r\n",adc_value[1]);
        printf("\r\n");
    }
}

/*!
    \brief      configure the different system clocks
    \param[in]  none
    \param[out] none
    \retval     none
*/
void rcu_config(void)
{
    /* enable GPIOC clock */
    rcu_periph_clock_enable(RCU_GPIOC);
    /* enable GPIOA clock */
    rcu_periph_clock_enable(RCU_GPIOA);  
    /* enable DMA clock */
    rcu_periph_clock_enable(RCU_DMA0);
    /* enable TIMER0 clock */
    rcu_periph_clock_enable(RCU_TIMER0);
    
}

/* retarget the C library printf function to the USART */
int fputc(int ch, FILE *f)
{
    usart_data_transmit(EVAL_COM1, (uint8_t) ch);
    while (RESET == usart_flag_get(EVAL_COM1,USART_FLAG_TBE));
    return ch;
}
