/*!
    \file  readme.txt
    \brief description of the CTC trim internal 48MHz RC oscillator with LXTAL clock
*/

/*
    Copyright (C) 2017 GigaDevice

    2017-05-19, V1.0.0, demo for GD32F30x
*/

  This example is based on the GD32303E-EVAL-V1.0 board, it shows CTC is used to trim internal
48MHz RC oscillator with LXTAL clock.

  Firstly, all the LEDs flash once for test. Then if the clock trim is OK, LED2 will be on.
Otherwise, all the LEDs are turned off.


