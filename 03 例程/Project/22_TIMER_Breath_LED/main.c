/*!
    \file  main.c
    \brief TIMER Breath LED demo
*/

/*
    Copyright (C) 2017 GigaDevice

    2017-05-19, V1.0.0, demo for GD32F30x
*/

#include "gd32f30x.h"
#include "gd32f303e_eval.h"
#include "systick.h"
#include "timer_pwm.h"


/*!
    \brief      main function
    \param[in]  none
    \param[out] none
    \retval     none
*/
int main(void)
{
    int16_t i = 0;
    FlagStatus breathe_flag = SET;
    
    
    /* configure the TIMER peripheral */
    timer_pwm_config();
    
    /* configure systick */
    systick_config();

    while (1){
        /* delay a time in milliseconds */
        delay_1ms(40);
        if (SET == breathe_flag){
             i = i + 10;
        }else{
            i = i - 10;
        }
        if(500 < i){
            breathe_flag = RESET;
        }
        if(0 >= i){
            breathe_flag = SET;
        }
        //配置TIMER通道输出脉冲值 也就是 CHX_CV值
        timer_channel_output_pulse_value_config(TIMER0,TIMER_CH_0,i);
    }
}
