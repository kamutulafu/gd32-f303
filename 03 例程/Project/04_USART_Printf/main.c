/*!
    \file  main.c
    \brief USART printf demo
*/

/*
    Copyright (C) 2017 GigaDevice

    2017-05-19, V1.0.0, demo for GD32F30x
*/

#include "gd32f30x.h"
#include "gd32f303e_eval.h"
#include "systick.h"
#include <stdio.h>

#include "led.h"
#include "key.h"
#include "uart.h"



void led_init(void);
void led_flash(int times);

/*!
    \brief      main function
    \param[in]  none
    \param[out] none
    \retval     none
*/
int main(void)
{
    /* initialize the LEDs */
    led_init();
    
    /* configure systick */
    systick_config();
    
    /* configure EVAL_COM1 */
    uart0_init();
    
    /* configure TAMPER key */
    key_init();
    
    /* output a message on hyperterminal using printf function */
    printf("\r\n USART printf example: please press the Tamper key \r\n");
    uart0_send_string("uart0_send_string \r\n");
	
    /* wait for completion of USART transmission */
    while(RESET == usart_flag_get(EVAL_COM1, USART_FLAG_TC)){
    }
    while(1){
        /* check if the tamper key is pressed */
        if(RESET == key_state_get()){
            delay_1ms(50);
            if(RESET == key_state_get()){
                delay_1ms(50);
                if(RESET == key_state_get()){
                    /* turn on LED2 */
                    led_on(1);
                    /* output a message on hyperterminal using printf function */
                    printf("\r\n USART printf example \r\n");
                    /* wait for completion of USART transmission */
                    while(RESET == usart_flag_get(EVAL_COM1, USART_FLAG_TC)){
                    }
                }else{
                    /* turn off LED2 */
                    led_off(1);
                }
            }else{
                /* turn off LED2 */
                led_off(1);
            }
        }else{
            /* turn off LED2 */
            led_off(1);
        }
    }
}


/* retarget the C library printf function to the USART */
int fputc(int ch, FILE *f)
{
    usart_data_transmit(USART0, (uint8_t)ch);
    while(RESET == usart_flag_get(USART0, USART_FLAG_TBE));
    return ch;
}
