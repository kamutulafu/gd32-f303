/*!
    \file  main.c
    \brief GPIO keyboard interrupt demo
*/

/*
    Copyright (C) 2017 GigaDevice

    2017-05-19, V1.0.0, demo for GD32F30x
*/

#include "gd32f30x.h"
#include "gd32f303e_eval.h"

#include "key.h"

/*!
    \brief      main function
    \param[in]  none
    \param[out] none
    \retval     none
*/
int main(void)
{  
    gd_eval_led_init(LED2);

	key_init();
	key_exit_mode_init();

	while(1){
    }
}
