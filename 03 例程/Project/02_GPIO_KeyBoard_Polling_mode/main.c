/*!
    \file  main.c
    \brief GPIO keyboard polling demo
*/

/*
    Copyright (C) 2017 GigaDevice

    2017-05-19, V1.0.0, demo for GD32F30x
*/

#include "gd32f30x.h"
#include "gd32f303e_eval.h"
#include "systick.h"

#include "led.h"
#include "key.h"

/*!
    \brief      main function
    \param[in]  none
    \param[out] none
    \retval     none
*/
int main(void)
{  
    key_init();
    systick_config();
    gd_eval_led_init(LED2);

    while(1){
		//判断下是否为低电平
        if(RESET == key_state_get()){
            /* 延迟50ms */
            delay_1ms(50);
			//再次判断是否为低电平
            if(RESET == key_state_get()){
				//LED 电平翻转
				//判断当前LED是否为低电平
				if(gpio_output_bit_get(LED2_GPIO_PORT, LED2_PIN) == RESET)
				{
					//输出高电平
					gpio_bit_write(LED2_GPIO_PORT, LED2_PIN, SET);
				}else{
					//输出低电平
					gpio_bit_write(LED2_GPIO_PORT, LED2_PIN, RESET);
				}
                
				//等待按键被松开
				while(RESET == key_state_get());
            }
        }
    }
}
