/*!
    \file  main.c
    \brief ADC Temperature Vrefint demo
*/

/*
    Copyright (C) 2017 GigaDevice

    2017-05-19, V1.0.0, demo for GD32F30x
*/

#include "gd32f30x.h"
#include "gd32f303e_eval.h"
#include "systick.h"
#include <stdio.h>

uint32_t adc_val_1;
uint32_t adc_val_2;

void rcu_config(void);
void adc_config(void);

/*!
    \brief      main function
    \param[in]  none
    \param[out] none
    \retval     none
*/
int main(void)
{
    /* configure systick */
    systick_config();  
    /* ADC configuration */
    adc_init();
    /* USART configuration */
    gd_eval_com_init(EVAL_COM1);

    while(1){
        /* ADC software trigger enable */
        adc_software_trigger_enable(ADC0, ADC_INSERTED_CHANNEL);
        /* delay a time in milliseconds */
        delay_1ms(2000);
      
        /* value convert  */
        adc_val_1 = ADC_IDATA0(ADC0); //pc3   IN3
        adc_val_2 = ADC_IDATA1(ADC0);//PC5   IN15
      
        /* value print */
        printf(" adc val 1 is 0x%x\r\n", adc_val_1);
        printf(" adc val 2 is 0x%x\r\n", adc_val_2);
        printf(" \r\n");
    }
}




/* retarget the C library printf function to the USART */
int fputc(int ch, FILE *f)
{
    usart_data_transmit(EVAL_COM1, (uint8_t) ch);
    while (RESET == usart_flag_get(EVAL_COM1,USART_FLAG_TBE));
    return ch;
}
