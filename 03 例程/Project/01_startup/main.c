/*!
    \file  main.c
    \brief GPIO running led
*/

/*
    Copyright (C) 2017 GigaDevice

    2017-05-19, V1.0.0, demo for GD32F30x
*/

#include "gd32f30x.h"
#include "gd32f303e_eval.h"
#include "systick.h"
#include "led.h"

/*!
    \brief      main function
    \param[in]  none
    \param[out] none
    \retval     none
*/
int main(void)
{  
    led_init();
    
    systick_config();
    
    while(1){
        /* turn on LED1 */
        led_on(0);
        /* insert 200 ms delay */
        delay_1ms(200);
        
        /* turn on LED2 */
        led_on(1);
        /* insert 200 ms delay */
        delay_1ms(200);
        
        /* turn on LED3 */
        led_on(2);
        /* insert 200 ms delay */
        delay_1ms(200);        

        /* turn off LEDs */
        led_off(0);
        led_off(1);
        led_off(2);
        
        /* insert 200 ms delay */
        delay_1ms(200);
    }
}
